package com.hal.predicurrency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PredicurrencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(PredicurrencyApplication.class, args);
	}
}
